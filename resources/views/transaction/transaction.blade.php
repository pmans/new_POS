@extends('layouts.master')
@section('content')
<link rel="stylesheet" type="text/css" href="/assets/css/select2.min.css">
<div class="container-fluid">
    <div class="side-body padding-top">

<div class="row">
  <div class="container-fluid">

                      <form class="form form-horizontal form-produk" method="post">
                        {{ csrf_field() }}  
                        <div class="col-md-6">
                          <div class="form-group label-floating">
                            <label class="control-label">Product Name</label>
                            <select name="category_id" class="form-control">
                              <optgroup>
                                @foreach($products as $product)

                                <option value="">{{ $product->product_name }}</option>
                                @endforeach

                              </optgroup>
                            </select>
                            <button type="submit" class="btn btn-info simpan"><i class="fa fa-plus"></i> Add to Transaction</button>
                          </div>
                        </div>
                      </form>

                      <form class="form-keranjang">
                        {{ csrf_field() }} {{ method_field('PATCH') }}
                        <table class="table table-striped tabel-penjualan">
                          <thead>
                           <tr>
                            <th width="30">No</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th align="right">Harga</th>
                            <th>Jumlah</th>
                            <th>Diskon</th>
                            <th align="right">Sub Total</th>
                            <th width="100">Aksi</th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                      </table>
                    </form>

                    <div class="col-md-8">
                     <div id="tampil-bayar" style="background: #dd4b39; color: #fff; font-size: 80px; text-align: center; height: 120px"></div>
                     <div id="tampil-terbilang" style="background: #3c8dbc; color: #fff; font-size: 25px; padding: 10px"></div>
                   </div>
                   <div class="col-md-4">
                    <form class="form form-horizontal form-penjualan" method="post" action="transaksi/simpan">
                      {{ csrf_field() }}
                      <input type="hidden" name="idpenjualan" value="">
                      <input type="hidden" name="total" id="total">
                      <input type="hidden" name="totalitem" id="totalitem">
                      <input type="hidden" name="bayar" id="bayar">

                      <div class="form-group">
                        <label for="totalrp" class="col-md-4 control-label">Total</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" id="totalrp" readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="member" class="col-md-4 control-label">Kode Member</label>
                        <div class="col-md-8">
                          <div class="input-group">
                            <input id="member" type="text" class="form-control" name="member" value="0">
                            <span class="input-group-btn">
                              <button onclick="showMember()" type="button" class="btn btn-info">...</button>
                            </span>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="diskon" class="col-md-4 control-label">Diskon</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="diskon" id="diskon" value="0" readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="bayarrp" class="col-md-4 control-label">Bayar</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" id="bayarrp" readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="diterima" class="col-md-4 control-label">Diterima</label>
                        <div class="col-md-8">
                          <input type="number" class="form-control" value="0" name="diterima" id="diterima">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="kembali" class="col-md-4 control-label">Kembali</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" id="kembali" value="0" readonly>
                        </div>
                      </div>

                    </form>
                  </div>

                </div>

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right simpan"><i class="fa fa-floppy-o"></i> Simpan Transaksi</button>
                </div>
              </div>
            </div>
          </div>

          @endsection