@extends('layouts.master')
@section('content')
<style type="text/css">
    .table td {
     text-align: center;   
 }
 .table th {
     text-align: center;   
 }
</style>
<div class="container-fluid">
    <div class="side-body padding-top">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">CATEGORY</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">                           
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#category">Add Category</button>
                                </div>
                            </div>  
                        </div>
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">CATEGORY</div>
                            <div class="panel-body">
                                <div class="col-lg-12">
                                </div>
                                <!-- Table -->

                                <table class="datatable table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Category Name</th>
                                            <th>Category On</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Category Name</th>
                                            <th>Category On</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($categories as $category)
                                        <tr>
                                            <th scope="row" width="20px">{{ $loop->index+1 }}</th>
                                            <td>{{ $category->category_name }}</td>
                                            <td>{{ $category->created_at }}</td>
                                            <td>
                                                <a href="category/{{ $category->id }}/delete-category" class="btn btn-danger btn-xs">
                                                    <span class="glyphicon glyphicon-remove"></span> Delete
                                                </a> 
                                                <a href=""><button type="submit" class="btn btn-primary btn-fill btn-xs">
                                                    <span class="glyphicon glyphicon-edit"></span> Edit</button>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('category.add-category')
@endsection
