<div class="modal fade modal-info" id="category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new Category</h4>
            </div>
            <form action="{{ route('add.category') }}" method="post" enctype="multipart/form-data" id="js-upload-form">
                {{csrf_field()}} 
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <input type="text" name="category_name" placeholder="Category" class="form-control">
                            </div>
                        </div>  
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Add Category</button>
                </form>
            </div>
        </div>
    </div>
</div>
