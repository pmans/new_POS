<!DOCTYPE html>
<html>

<head>
    <title>Point Of Sales Pondok IT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- CSS Libs -->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-switch.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/checkbox3.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/select2.min.css">
    <!-- CSS App -->
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/flat-blue.css">
</head>

<body class="flat-blue">

    @include('layouts.navbar')
    @include('layouts.sidebar')

    @yield('content')

</body>
    <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/Chart.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.matchHeight-min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/select2.full.min.js"></script>
    <script type="text/javascript" src="/assets/js/ace/ace.js"></script>
    <script type="text/javascript" src="/assets/js/ace/mode-html.js"></script>
    <script type="text/javascript" src="/assets/js/ace/theme-github.js"></script>
    <!-- Javascript -->
    <script type="text/javascript" src="/assets/js/app.js"></script>
    <script type="text/javascript" src="/assets/js/index.js"></script>