@if (session('info'))
    <div class="alert alert-info">
        <b><span class="glyphicon glyphicon-ok"></span> {{ session('info') }}</b>
    </div>
@endif

@if (session('danger'))
    <div class="alert alert-danger">
       <span class="glyphicon glyphicon-ok"></span><b> {{ session('danger') }}</b>
    </div>
@endif