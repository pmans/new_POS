<!DOCTYPE html>
<html>
<head>
  <title>Point Of Sales Pondok IT</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
  <!-- CSS Libs -->
  <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/animate.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-switch.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/checkbox3.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/select2.min.css">
  <!-- CSS App -->
  <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/flat-blue.css">
</head>

<body class="flat-blue">
  <div class="wrapper body-inverse"> <!-- wrapper -->
    <div class="container">
      <div class="row">
        <!-- Sign In form -->

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="col-sm-5 col-sm-offset-1">
              <h3><center>Sign In to your account</center></h3>
              <p class="text-muted">
                Please fill out the form below to login to your account.
              </p>
              <div class="form-white">

                <form method="POST" action="{{ route('login') }}">
                  {{ csrf_field() }}

                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">E-Mail Address</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password</label>
                    <input id="password" type="password" class="form-control" name="password" required>
                    @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                      </label>
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-block btn-info btn-xxl">Login</button>
                  </div>
                  
                </form>
              </div>
            </div>


            <div class="col-sm-5">
              <h3 class="text-right-xs"><center>Sign Up to your account</center></h3>
              <p class="text-muted text-right-xs">
                Please fill out the form below to create a new account.
              </p>
              <div class="form-white">
                <form method="POST" action="{{ route('register') }}">
                  {{ csrf_field() }}

                  <div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">
                    <label for="business_name">Business Name</label>
                    <input type="text" class="form-control" name="business_name" id="business_name" placeholder="Business name" value="{{ old('business_name') }}" required autofocus>
                    @if ($errors->has('business_name'))
                    <span class="help-block">
                      <strong>{{ $errors->first('business_name') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Full Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Your name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                    <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                  </div>


                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="name">E-Mail Adress</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
                    @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                    <label for="phone_number">Phone Number</label>
                    <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="+62">
                    @if ($errors->has('phone_number'))
                    <span class="help-block">
                      <strong>{{ $errors->first('phone_number') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="password2">Password</label>
                          <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                          @if ($errors->has('password'))
                          <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                          @endif
                        </div>

                        <div class="col-sm-6">
                          <label for="password">Repeat password</label>
                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-info btn-xxl">Create an account</button>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div> <!-- / wrapper -->
      </body>

      <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="/assets/js/Chart.min.js"></script>
      <script type="text/javascript" src="/assets/js/bootstrap-switch.min.js"></script>
      <script type="text/javascript" src="/assets/js/jquery.matchHeight-min.js"></script>
      <script type="text/javascript" src="/assets/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="/assets/js/dataTables.bootstrap.min.js"></script>
      <script type="text/javascript" src="/assets/js/select2.full.min.js"></script>
      <script type="text/javascript" src="/assets/js/ace/ace.js"></script>
      <script type="text/javascript" src="/assets/js/ace/mode-html.js"></script>
      <script type="text/javascript" src="/assets/js/ace/theme-github.js"></script>
      <!-- Javascript -->
      <script type="text/javascript" src="/assets/js/app.js"></script>
      <script type="text/javascript" src="/assets/js/index.js"></script>