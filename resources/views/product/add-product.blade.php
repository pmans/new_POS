<style type="text/css">
    .modal-open .select2-container--open { z-index: 999999 !important; width:100% !important; }
</style>

<div class="modal fade modal-info" id="product" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new Product</h4>
            </div>
            <form action="{{ route('add.product') }}" method="post" enctype="multipart/form-data" id="js-upload-form">
                {{csrf_field()}}      
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating has-feedback{{ $errors->has('product_name') ? ' has-error' : '' }}">
                                <label class="control-label">Produk Name</label>
                                <input type="text" name="product_name" value="{{ old('product_name') }}" class="form-control">
                                @if ($errors->has('product_name'))
                                <span class="help-block">
                                    <p><b>{{ $errors->first('product_name') }}</b></p>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Category</label><p>
                                <select name="category_id" class="form-control">
                                    @foreach($category as $categories)
                                    <option value="{{ $categories->id }}">{{ $categories->category_name }}
                                    </option>
                                    @endforeach                                   
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group label-floating has-feedback{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="control-label">Description</label>
                                <textarea name="description" class="form-control" rows="3">{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                <span class="help-block">
                                    <p><b>{{ $errors->first('description') }}</b></p>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating has-feedback{{ $errors->has('sku') ? ' has-error' : '' }}">
                                <label class="control-label">SKU</label>
                                <input type="text" name="sku" value="{{ old('sku') }}" class="form-control">
                                @if ($errors->has('sku'))
                                <span class="help-block">
                                    <p><b>{{ $errors->first('sku') }}</b></p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group label-floating has-feedback{{ $errors->has('sku') ? ' has-error' : '' }}">
                                <label class="control-label">Price</label>
                                <input type="text" name="price" value="{{ old('price') }}" class="form-control">
                                @if ($errors->has('price'))
                                <span class="help-block">
                                    <p><b>{{ $errors->first('price') }}</b></p>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Add Product</button>
                </form>
            </div>
        </div>
    </div>
</div>
