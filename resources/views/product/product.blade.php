@extends('layouts.master')
@section('content')

<style type="text/css">
   .table td {
       text-align: center;   
   }
   .table th {
       text-align: center;   
   }
</style>

<div class="container-fluid">
    <div class="side-body padding-top">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">PRODUCT</div>
                        </div>
                    </div>
                    <div class="card-body">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">                           
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#product">Add Product</button>
                                </div>
                            </div>  
                        </div>
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">PRODUCT</div>
                            <div class="panel-body">
                                <div class="col-lg-12">

                                    <p>Some default panel content here. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                </div>
                                <!-- Table -->
                                <table class="datatable table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product Name</th>
                                            <th>Category</th>
                                            <th>Description</th>
                                            <th>SKU</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($products as $product)
                                        <tr>
                                            <th>{{ $loop->index+1 }}</th>
                                            <td>{{ $product->product_name }}</td>
                                            <td>{{ $product->category->category_name }}</td>
                                            <td>{{ $product->description }}</td>
                                            <td>{{ $product->sku }}</td>
                                            <td>{{ $product->price }}</td>
                                            <td>
                                                <a href="product/{{ $product->id }}/delete-product" class="btn btn-danger btn-xs">
                                                    <span class="glyphicon glyphicon-remove"></span> Delete
                                                </a> 
                                                <a href=""><button type="submit" class="btn btn-primary btn-fill btn-xs">
                                                   <span class="glyphicon glyphicon-edit"></span> Edit</button>
                                               </a>
                                           </td>
                                       </tr>
                                       @endforeach 
                                   </tbody>
                               </table>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
@include('product.add-product')
@endsection