<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'product' ], function(){
    Route::get('/', 'ProductController@index')->name('product');
    Route::get('/', 'ProductController@show')->name('product');
    Route::post('/add', 'ProductController@store')->name('add.product');
    Route::get('/{id}/delete-product', 'ProductController@destroy')->name('delete.product');
});

Route::group(['prefix' => 'category' ], function(){
Route::get('/', 'CategoryController@index')->name('category');
Route::get('/', 'CategoryController@show')->name('category');
Route::post('/add', 'CategoryController@store')->name('add.category');
Route::get('/{id}/delete-category', 'CategoryController@destroy')->name('delete.product');
});


Route::group(['prefix' => 'transaction' ], function(){
Route::get('/', 'TransactionController@index')->name('transaction');
});
