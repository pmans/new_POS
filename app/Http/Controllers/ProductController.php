<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{

    private $product, $category;

    public function __construct()
    {
        $this->product = Product::all();
        $this->category = Category::all();
    }

    public function index()
    {
        return view('product.product');
    }

    public function store()
    {
        $this->validate(request(), [
            'product_name'  => 'required',
            'description'   => 'required',
            'sku'           => 'required',
            'price'         => 'required'
        ]);

        Product::create([
            'product_name'  => request('product_name'),
            'category_id'   => request('category_id'),
            'description'   => request('description'),
            'sku'           => request('sku'),
            'price'         => request('price')
            ]);
        return redirect('product')->withInfo('Product Successfully Add!!!');
    }
     public function show()
    {
        $category = Category::all();
        $products = \App\Product::all();
        return view('product.product', compact('products','category')); 
    }
}
