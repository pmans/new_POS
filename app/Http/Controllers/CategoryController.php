<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return view('category.category');
    }

    public function store()
    {
        $this->validate(request(), [
            'category_name'  => 'required'
        ]);

        Category::create([
            'category_name'  => request('category_name')
            ]);
        return redirect('category')->withInfo('Product Successfully Add!!!');
    }

    public function show()
    {
        $categories = \App\Category::all();
        return view('category.category', compact('categories')); 
    }

    public function destroy($id)
    {
        $categories = Category::find($id)->delete();
        return redirect('category')->withDanger('Explicit Successfully Delete!!!');
    }

}
