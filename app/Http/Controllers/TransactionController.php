<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class TransactionController extends Controller
{
    public function index()
    {
        $products = \App\Product::all();
        return view('transaction.transaction', compact('products')); 
    }

}
