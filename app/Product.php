<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['product_name','category_id','description','sku','price'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
